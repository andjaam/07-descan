#ifndef DIALOGMAIL_H
#define DIALOGMAIL_H

#include <QObject>
#include <QDebug>
#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QString>
#include <curl/curl.h>
#include <iostream>

namespace Ui {
class DialogMail;
}

class DialogMail : public QDialog {
  Q_OBJECT

public:
  explicit DialogMail(QWidget *parent = nullptr, QStringList filePathsPdf = {});
  auto mailSender(QString &recipient, QString &subject, QString &message);
  ~DialogMail();

  QStringList m_filePathsPdf;

private slots:
  void on_pbExit_clicked();
  void on_pbBrowse_clicked();
  void on_pbSend_clicked();

private:
  Ui::DialogMail *ui;
};

#endif // DIALOGMAIL_H
